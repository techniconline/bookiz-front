<?php include('layouts/head.php'); ?>
<?php include('layouts/header.php'); ?>
    <div class="list-page">
        <div class="entities">
            <?php for($i = 0; $i < 5 ; $i++): ?>
                <div class="entity" data-map='{   
                            "id": <?= $i; ?>, 
                            "image": "http://beautyday.ir/storage/www/default.jpg",
                            "title": "سالن زیبایی الیناز",
                            "address": "تهران خیابان مطهری بعد از تقاطع سهروردی روبروی مرکز آموزشی و همایش بانک سپه نبش بن بست فیروزه پلاک 1 واحد 3",
                            "latitude": "35.72<?= $i; ?>95<?= $i; ?>00", 
                            "longitude": "51.44<?= $i; ?>26<?= $i; ?>00"
                        }'>
                
                    <div class="img-container">
                        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSA3HKr1UK4l4OzzH6ceze3KWWdHpNDYKYd6A6WotDFRU_pZvm1" alt="" />
                        <div class="rate">
                            <i class="fas fa-star active"></i>
                            <i class="fas fa-star active"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                    </div>
                    <div class="details">
                        <h3>Zasman vet stroud green</h3>
                        <p class="address">162 Tolington park london n4 3AD</p>
                        <a href="#" class="more">
                            MORE
                            <i class="fas fa-chevron-left"></i>
                        </a>
                    </div>
                </div>
            <?php endfor; ?>
            <div class="pagination-list">
                <button id="nextPage" class="btn btn-dark btn-block">SHOW MORE</button>
            </div>
        </div>
        <div id='map'></div>
    </div>
<?php include('layouts/foot.php'); ?>