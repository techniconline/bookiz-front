<?php include('layouts/head.php'); ?>
<?php include('layouts/header.php'); ?>
<div class="single-page">
    <div class="single-content">
        <div class="slider owl-carousel owl-theme owl-2">
            <div class="item">
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTE9gNOnOeFQEB1c9_05suIqW9Rde_rZbkyOr95Ws7b1tfvA_2F" />
            </div>
            <div class="item">
                <img src="https://www.biltmorehotel.com/assets/img/spa/slider/mb/the-biltmore-spa-miami.jpg" />
            </div>
        </div>
        <div class="details">
            <div class="info">
                <h1>Coco / NVmyBeauty Nottingham</h1>
                <div class="rate">
                    <span class="num">4.0</span>
                    <div class="stars">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="far fa-star"></i>
                    </div>
                </div>
            </div>
            <h4 class="address">at Coco Hair Beauty Nails, 138 Hartley Road, Nottingham, NG7 3AJ</h4>
            <p class="discount">
                <i class="fas fa-percent"></i>
                Off-peak discounts
            </p>

        </div>
        <ul class="services-list">
            <li data-id="1" data-price="35">
                <a href="#">
                    <span class="info">
                        <span class="title">Ladies - Olaplex Conditioning Treatment with Wash & Blow Dry</span>
                        <span class="time">1 hr 40 mins</span>
                    </span>
                    <span class="prices">
                        <span class="new-price">35$</span>
                        <span class="old-price">55$</span>
                    </span>
                    <span class="select">SELECT</span>
                </a>
            </li>
            <li data-id="2" data-price="35">
                <a href="#">
                    <span class="info">
                        <span class="title">Ladies - Olaplex Conditioning Treatment with Wash & Blow Dry</span>
                        <span class="time">1 hr 40 mins</span>
                    </span>
                    <span class="prices">
                        <span class="new-price">35$</span>
                        <span class="old-price">55$</span>
                    </span>
                    <span class="select">SELECT</span>
                </a>
            </li>
            <li data-id="3" data-price="35">
                <a href="#">
                    <span class="info">
                        <span class="title">Ladies - Olaplex Conditioning Treatment with Wash & Blow Dry</span>
                        <span class="time">1 hr 40 mins</span>
                    </span>
                    <span class="prices">
                        <span class="new-price">35$</span>
                    </span>
                    <span class="select">SELECT</span>
                </a>
            </li>
        </ul>

        <div class="services">
            <div class="categories-container">
                <ul class="categories">
                    <li data-type="all" class="active">
                        <img src="./assets/images/svg-icons/all.svg" />
                        <img class="img-active" src="./assets/images/svg-icons/all-active.svg" />
                        <span>All</span>
                    </li>
                    <li data-type="hair">
                        <img src="./assets/images/svg-icons/barber.svg" />
                        <img class="img-active" src="./assets/images/svg-icons/barber-active.svg" />
                        <span>Hair</span>
                    </li>
                    <li data-type="nail">
                        <img src="./assets/images/svg-icons/nail.svg" />
                        <img class="img-active" src="./assets/images/svg-icons/nail-active.svg" />
                        <span>Nails</span>
                    </li>
                    <li data-type="face">
                        <img src="./assets/images/svg-icons/face.svg" />
                        <img class="img-active" src="./assets/images/svg-icons/face-active.svg" />
                        <span>Face</span>
                    </li>
                    <li data-type="massage">
                        <img src="./assets/images/svg-icons/massage.svg" />
                        <img class="img-active" src="./assets/images/svg-icons/massage-active.svg" />
                        <span>Massage</span>
                    </li>
                    <li data-type="body">
                        <img src="./assets/images/svg-icons/body.svg" />
                        <img class="img-active" src="./assets/images/svg-icons/body-active.svg" />
                        <span>Body</span>
                    </li>
                </ul>
            </div>
            <div class="category-contents">
                <div class="category-content active" data-category="all">
                    <ul class="nav nav-tabs sub-categories">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#home">
                                <span class="text">Ladies - Haircuts & Hairdressing</span>
                                <span class="num">(9)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#menu1">
                                <span class="text">Ladies - Hair Colouring & Highlights</span>
                                <span class="num">(6)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#menu2">
                                <span class="text">Manicures & Pedicures</span>
                                <span class="num">(10)</span>
                            </a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="home">
                            <ul class="services-list">
                                <li data-id="1" data-price="35">
                                    <a href="#">
                                        <span class="info">
                                            <span class="title">Ladies - Olaplex Conditioning Treatment with Wash & Blow Dry</span>
                                            <span class="time">1 hr 40 mins</span>
                                        </span>
                                        <span class="prices">
                                            <span class="new-price">35$</span>
                                            <span class="old-price">55$</span>
                                        </span>
                                        <span class="select">SELECT</span>
                                    </a>
                                </li>
                                <li data-id="2" data-price="35">
                                    <a href="#">
                                        <span class="info">
                                            <span class="title">Ladies - Olaplex Conditioning Treatment with Wash & Blow Dry</span>
                                            <span class="time">1 hr 40 mins</span>
                                        </span>
                                        <span class="prices">
                                            <span class="new-price">35$</span>
                                            <span class="old-price">55$</span>
                                        </span>
                                        <span class="select">SELECT</span>
                                    </a>
                                </li>
                                <li data-id="3" data-price="35">
                                    <a href="#">
                                        <span class="info">
                                            <span class="title">Ladies - Olaplex Conditioning Treatment with Wash & Blow Dry</span>
                                            <span class="time">1 hr 40 mins</span>
                                        </span>
                                        <span class="prices">35$</span>
                                        <span class="select">SELECT</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-pane container" id="menu1">2</div>
                        <div class="tab-pane container" id="menu2">3</div>
                    </div>
                </div>
                <div class="category-content" data-category="hair">
                    hair
                </div>
                <div class="category-content" data-category="nail">
                    nail
                </div>
                <div class="category-content" data-category="face">
                    face
                </div>
                <div class="category-content" data-category="massage">
                    massage
                </div>
                <div class="category-content" data-category="body">
                    body
                </div>
            </div>
        </div>

        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="info-tab" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="false">Info</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="comments-tab" data-toggle="tab" href="#comments" role="tab" aria-controls="comments" aria-selected="true">Comments</a>
            </li>
            
            <li class="nav-item">
                <a class="nav-link" id="gallery-tab" data-toggle="tab" href="#gallery" role="tab" aria-controls="gallery" aria-selected="false">Gallery</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="info" role="tabpanel" aria-labelledby="info-tab">
                <?php include('components/info.php'); ?>
            </div>
            <div class="tab-pane fade show" id="comments" role="tabpanel" aria-labelledby="comments-tab">
                <?php include('components/comments.php'); ?>
            </div>
            <div class="tab-pane fade" id="gallery" role="tabpanel" aria-labelledby="gallery-tab">
                <?php include('components/gallery.php'); ?>
            </div>
        </div>
    </div>
    <div class="single-content p-3">
        <?php include("components/slider.php"); ?>
    </div>
    <div class="basket">
        <div class="basket-info">
            <span class="count">0</span>
            <span class="text">Services</span>
            <span class="price">$160</span>
        </div>
        <a href="checkout.html" class="link-basket">Choose Time</a>
    </div>
</div>

<?php include('layouts/footer.php'); ?>
<?php include('layouts/foot.php'); ?>