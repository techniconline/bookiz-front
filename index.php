<?php include('layouts/head.php'); ?>
<?php include('layouts/header.php'); ?>
<div class="home-page">
    <div class="home-head">
        <!--<img src="./assets/images/home_head.png" alt="" />-->
        <div id="carousel" class="carousel slide" data-ride="carousel">

            <!-- Indicators -->
            <ul class="carousel-indicators">
                <li data-target="#carousel" data-slide-to="0" class="active"></li>
                <li data-target="#carousel" data-slide-to="1"></li>
                <li data-target="#carousel" data-slide-to="2"></li>
            </ul>

            <!-- The slideshow -->
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="./assets/images/home_head.png" alt="Los Angeles">
                    <div class="carousel-caption">
                        <h3>Los Angeles</h3>
                        <p>We had such a great time in LA!</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="./assets/images/home_head.png" alt="Chicago">
                    <div class="carousel-caption">
                        <h3>Los Angeles</h3>
                        <p>We had such a great time in LA!</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="./assets/images/home_head.png" alt="New York">
                    <div class="carousel-caption">
                        <h3>Los Angeles</h3>
                        <p>We had such a great time in LA!</p>
                    </div>
                </div>
            </div>

            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#carousel" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#carousel" data-slide="next">
                <span class="carousel-control-next-icon"></span>
            </a>

        </div>
        <div class="content">
            <?php include('components/advance-search.php'); ?>
        </div>
    </div>
    <div class="section-gray">
        <div class="content">
            <h3 class="content-title">BEAUTY SALONS NEARBY</h3>
            <div class="mini-slider">
                <div class="owl-carousel owl-5">
                    <div class="item card-white">
                        <img class="owl-lazy" data-src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTE9gNOnOeFQEB1c9_05suIqW9Rde_rZbkyOr95Ws7b1tfvA_2F" />
                        <h3>L’Oréal H&M Hair & Beauty S</h3>
                        <p class="distance">0.3 mi away</p>
                        <div class="rate">
                            <div class="stars">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                            </div>
                            <div class="num">3.8</div>
                        </div>
                    </div>
                    <div class="item card-white">
                        <img class="owl-lazy" data-src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTE9gNOnOeFQEB1c9_05suIqW9Rde_rZbkyOr95Ws7b1tfvA_2F" />
                        <h3>L’Oréal H&M Hair & Beauty S</h3>
                        <p class="distance">0.3 mi away</p>
                        <div class="rate">
                            <div class="stars">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                            </div>
                            <div class="num">3.8</div>
                        </div>
                    </div>
                    <div class="item card-white">
                        <img class="owl-lazy" data-src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTE9gNOnOeFQEB1c9_05suIqW9Rde_rZbkyOr95Ws7b1tfvA_2F" />
                        <h3>L’Oréal H&M Hair & Beauty S</h3>
                        <p class="distance">0.3 mi away</p>
                        <div class="rate">
                            <div class="stars">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                            </div>
                            <div class="num">3.8</div>
                        </div>
                    </div>
                    <div class="item card-white">
                        <img class="owl-lazy" data-src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTE9gNOnOeFQEB1c9_05suIqW9Rde_rZbkyOr95Ws7b1tfvA_2F" />
                        <h3>L’Oréal H&M Hair & Beauty S</h3>
                        <p class="distance">0.3 mi away</p>
                        <div class="rate">
                            <div class="stars">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                            </div>
                            <div class="num">3.8</div>
                        </div>
                    </div>
                    <div class="item card-white">
                        <img class="owl-lazy" data-src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTE9gNOnOeFQEB1c9_05suIqW9Rde_rZbkyOr95Ws7b1tfvA_2F" />
                        <h3>L’Oréal H&M Hair & Beauty S</h3>
                        <p class="distance">0.3 mi away</p>
                        <div class="rate">
                            <div class="stars">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                            </div>
                            <div class="num">3.8</div>
                        </div>
                    </div>
                    <div class="item card-white">
                        <img class="owl-lazy" data-src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTE9gNOnOeFQEB1c9_05suIqW9Rde_rZbkyOr95Ws7b1tfvA_2F" />
                        <h3>L’Oréal H&M Hair & Beauty S</h3>
                        <p class="distance">0.3 mi away</p>
                        <div class="rate">
                            <div class="stars">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                            </div>
                            <div class="num">3.8</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section-white">
        <div class="content">
            <div class="big-cards">
                <div class="big-card">
                    <img src="https://cdn1.treatwell.net/images/view/v2.i1513595.w540.h192.xDF38EF5F.png" alt="" />
                    <div class="detail">
                        <h3>Download our app</h3>
                        <p class="desc">Book treatments and find the best salons near you with a quick swipe or two.</p>
                        <button type="button" class="btn btn-dark">GET IT NOW</button>
                    </div>
                </div>
                <div class="big-card">
                    <img src="https://cdn1.treatwell.net/images/view/v2.i1513595.w540.h192.xDF38EF5F.png" alt="" />
                    <div class="detail">
                        <h3>Download our app</h3>
                        <p class="desc">Book treatments and find the best salons near you with a quick swipe or two.</p>
                        <button type="button" class="btn btn-dark">GET IT NOW</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section-gray">
        <div class="content">
            <h3 class="content-title">BROWSE BY CITY</h3>
            <ul class="cities">
                <li><a href="">London</a></li>
                <li><a href="">Edinburgh</a></li>
                <li><a href="">Glasgow</a></li>
                <li><a href="">Birmingham</a></li>
                <li><a href="">Leeds</a></li>
                <li><a href="">Manchester</a></li>
                <li><a href="">Liverpool</a></li>
                <li><a href="">Bristol</a></li>
            </ul>
        </div>
    </div>
</div>
<?php include('layouts/footer.php'); ?>
<?php include('layouts/foot.php'); ?>