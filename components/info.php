<div class="about">
    <h3>ABOUT</h3>
    <div class="content">
        <div id='map'></div>
        <div class="body">
            <h3 class="title">Coco / NVmyBeauty Nottingham</h3>
            <p class="address">at Coco Hair Beauty Nails</p>
            <p class="address">138 Hartley Road</p>
            <p class="address">Nottingham</p>
            <p class="address">NG7 3AJ</p>
        </div>
    </div>
</div>

<div class="others">
    <div class="description">
        <p>NVmyBeauty, found in Nottingham, specialise in beauty and hair services such as hair styling, cutting, colouring, facials, manicures and pedicures.</p>
        <p>The staff in this friendly and warm hair and beauty salon have been giving great results since 2014. The therapists are warm, approachable and very knowledgeable and they love making you feel welcome and at home.</p>
        <p>The girls have over ten years of experience and only use the most proven brands such as L'Oreal, Shellac, LVL, HD Brows, Moroccan Oil and Crystal Clear so you leave looking as good as you feel.</p>
        <p>The salon is accessible via wheelchair and pram, with parking in the retail park just across the road so there is nothing stopping you from enjoying their warm and welcoming treatments.</p>
        <p>Whether you're after a complete makeover or just a little something to make your day special, these girls are willing to offer their expert opinion and put a smile on your face.</p>
    </div>
    <ul class="salon-times">
        <li class="closed">
            <span class="day">
                <span class="badge"></span>
                <span class="text">Monday</span>
            </span>
            <span class="time">Closed</span>
        </li>
        <li>
            <span class="day">
                <span class="badge"></span>
                <span class="text">Tuesday</span>
            </span>
            <span class="time">10:00 AM	– 7:00 PM</span>
        </li>
        <li>
            <span class="day">
                <span class="badge"></span>
                <span class="text">Tuesday</span>
            </span>
            <span class="time">10:00 AM	– 7:00 PM</span>
        </li>
        <li>
            <span class="day">
                <span class="badge"></span>
                <span class="text">Tuesday</span>
            </span>
            <span class="time">10:00 AM	– 7:00 PM</span>
        </li>
    </ul>
</div>