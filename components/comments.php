<div class="ratings">
    <div class="total-rate">
        <span class="num">4.0</span>
        <div>
            <div class="stars">
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="far fa-star"></i>
            </div>
            <span class="review-count">14 reviews</span>
        </div>
    </div>
    <div class="detail-rate">
        <div class="item">
            <div class="title">Ambience</div>
            <div class="stars">
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="far fa-star"></i>
            </div>
        </div>
        <div class="item">
            <div class="title">Staff</div>
            <div class="stars">
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="far fa-star"></i>
            </div>
        </div>
        <div class="item">
            <div class="title">Cleanliness</div>
            <div class="stars">
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="far fa-star"></i>
            </div>
        </div>
        <div class="item">
            <div class="title">Value</div>
            <div class="stars">
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="far fa-star"></i>
            </div>
        </div>
    </div>
</div>

<div class="comments">
    <?php for($c = 0 ; $c < 5 ; $c++): ?>
        <div class="comment">
            <div class="user">
                <img src="https://st2.depositphotos.com/3400509/12022/v/950/depositphotos_120221752-stock-illustration-black-and-white-woman-avatar.jpg" alt="">
                <div class="data">
                    <span class="name">Sarbjeet</span>
                    <span class="time">12 hours ago</span>
                </div>
            </div>
            <p class="content">This was the first time I had the treatment. I quite enjoyed and my skin felt good and hydrated.</p>
        </div>
    <?php endfor; ?>
</div>

<!-- If user Logged In -->
<form class="send-comment">
    <img src="https://st2.depositphotos.com/3400509/12022/v/950/depositphotos_120221752-stock-illustration-black-and-white-woman-avatar.jpg" alt="">
    <div class="input-container">
        <textarea class="form-control" id="comment" placeholder="writer a comment"></textarea>
        <button type="submit" class="btn btn-dark btn-sm">Send</button>
    </div>
</form>