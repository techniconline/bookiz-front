<div class="mini-slider">
    <h3>BEAUTY SALONS NEARBY</h3>
    <div class="owl-carousel owl-5">
        <?php for($o = 0 ; $o < 10 ; $o++): ?>
        <div class="item">
            <img class="owl-lazy" data-src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTE9gNOnOeFQEB1c9_05suIqW9Rde_rZbkyOr95Ws7b1tfvA_2F" />
            <h3>L’Oréal H&M Hair & Beauty S</h3>
            <!-- <p class="distance">0.3 mi away</p> -->
            <div class="rate">
                <div class="stars">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="far fa-star"></i>
                </div>
                <div class="num">3.8</div>
            </div>
        </div>
        <?php endfor;?>
    </div>
</div>