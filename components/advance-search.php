<div class="search-box-container">
    <button class="search-box-btn btn btn-dark btn-sm" data-toggle="modal" data-target="#searchBoxModal">ADVANCE SEARCH</button>
    <div class="search-box">
        <ul class="tabs">
            <li data-type="treatments" class="active">Treatments</li>
            <li data-type="spa">Spa</li>
            <li data-type="salon">Salon</li>
        </ul>
        <form class="box active" data-type="treatments">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Search for treatments" />
            </div>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Enter Area" />
            </div>
            <button type="button" class="btn btn-dark btn-block">Search</button>
        </form>
        <form class="box" data-type="spa">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="City Or Region" />
            </div>
            <div class="form-group">
                <input type="number" class="form-control" placeholder="Any Duration" />
            </div>
            <button type="button" class="btn btn-dark btn-block">Search</button>
        </form>
        <form class="box" data-type="salon">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Search By Salon Name" />
            </div>
            <button type="button" class="btn btn-dark btn-block">Search</button>
        </form>
    </div>
</div>
<div class="auth-modal modal fade" id="searchBoxModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title">ADVANCE SEARCH</h5>
      </div>
      <div class="modal-body search-box-modal">
        <ul class="tabs">
            <li data-type="treatments" class="active">Treatments</li>
            <li data-type="spa">Spa</li>
            <li data-type="salon">Salon</li>
        </ul>
        <form class="box active" data-type="treatments">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Search for treatments" />
            </div>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Enter Area" />
            </div>
            <button type="button" class="btn btn-dark btn-block">Search</button>
        </form>
        <form class="box" data-type="spa">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="City Or Region" />
            </div>
            <div class="form-group">
                <input type="number" class="form-control" placeholder="Any Duration" />
            </div>
            <button type="button" class="btn btn-dark btn-block">Search</button>
        </form>
        <form class="box" data-type="salon">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Search By Salon Name" />
            </div>
            <button type="button" class="btn btn-dark btn-block">Search</button>
        </form>
      </div>
    </div>
  </div>
</div>
