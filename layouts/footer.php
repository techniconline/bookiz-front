
<div class="footer">
    <div class="footer-container">
        <div class="main-footer row">
            <div class="col-xs-12 col-md-3">
                <h4>Contact</h4>
                <ul>
                    <li><a href="#">Help / Contact Us</a></li>
                    <li><a href="#"><img src="https://www.treatwell.co.uk/images/view/v2.i1500465.w-1.h-1.x9F6BAEC2.svg" alt="" /></a></li>
                    <li><a href="#"><img src="https://www.treatwell.co.uk/images/view/v2.i1500466.w-1.h-1.x5D94FDB5.svg" alt="" /></a></li>
                </ul>
            </div>
            <div class="col-xs-12 col-md-3">
                <h4>Discover</h4>
                <ul>
                    <li><a href="#">Treatment Guide</a></li>
                    <li><a href="#">The Treatment Files</a></li>
                    <li><a href="#">Treatwell Gift Card</a></li>
                    <li><a href="#">Sign up for our newsletter</a></li>
                </ul>
            </div>
            <div class="col-xs-12 col-md-3">
                <h4>Partners</h4>
                <ul>
                    <li><a href="#">List your business</a></li>
                    <li><a href="#">Partner Help Centre</a></li>
                    <li><a href="#">The Treatwell Academy</a></li>
                </ul>
            </div>
            <div class="col-xs-12 col-md-3">
                <h4>Company</h4>
                <ul>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">We are Hiring</a></li>
                    <li><a href="#">Legal</a></li>
                    <li><a href="#">Tax strategy</a></li>
                </ul>
            </div>
        </div>
        <div class="bottom-footer">
            <ul class="socials">
                <li>
                    <a href="#"><i class="fab fa-instagram"></i></a>
                </li>
                <li>
                    <a href="#"><i class="fab fa-twitter"></i></a>
                </li>
                <li>
                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                </li>
                <li>
                    <a href="#"><i class="fab fa-youtube"></i></a>
                </li>
            </ul>
        </div>
    </div>
</div>
