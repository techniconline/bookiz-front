
        <script defer src="https://use.fontawesome.com/releases/v5.8.1/js/all.js" integrity="sha384-g5uSoOSBd7KkhAMlnQILrecXvzst9TdC09/VM+pjDTCM+1il8RHz5fKANTFFb+gQ" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" ></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/jsCalendar.js"></script>
        <script src="assets/js/masonry.min.js"></script>
        <script src="assets/js/owl.carousel.min.js"></script>
        <script src="assets/js/components.js"></script>
        <script src="assets/js/main.js"></script>
    </body>
</html>