<div class="header-container page-header">
    <div class="header">
        <div class="logo-container">
            <img src="./assets/images/logo.png" alt="" />
        </div>
        <div class="search-container">
            <i class="fa fa-search"></i>
            <input type="search" />
            <!-- <ul class="autocomplete-data">
                <li class="head"><a href="#">Results</a></li>
                <li><a href="#">Auto Complete</a></li>
                <li><a href="#">Auto Complete</a></li>
                <li><a href="#">Auto Complete</a></li>
                <li><a href="#">Auto Complete</a></li>
                <li><a href="#">Auto Complete</a></li>
                <li><a href="#">Auto Complete</a></li>
                <li><a href="#">Auto Complete</a></li>
            </ul> -->
            <div class="autocomplete">
                <ul>
                    <li class="head"><a href="#">Auto Complete</a></li>
                    <li><a href="#">Auto Complete</a></li>
                    <li><a href="#">Auto Complete</a></li>
                    <li><a href="#">Auto Complete</a></li>
                    <li><a href="#">Auto Complete</a></li>
                    <li><a href="#">Auto Complete</a></li>
                </ul>
                <ul>
                    <li class="head"><a href="#">Auto Complete</a></li>
                    <li><a href="#">Auto Complete</a></li>
                    <li><a href="#">Auto Complete</a></li>
                    <li><a href="#">Auto Complete</a></li>
                    <li><a href="#">Auto Complete</a></li>
                    <li><a href="#">Auto Complete</a></li>
                </ul>
                <ul>
                    <li class="head"><a href="#">Auto Complete</a></li>
                    <li><a href="#">Auto Complete</a></li>
                    <li><a href="#">Auto Complete</a></li>
                    <li><a href="#">Auto Complete</a></li>
                    <li><a href="#">Auto Complete</a></li>
                    <li><a href="#">Auto Complete</a></li>
                </ul>
            </div>
        </div>
        <button id="head-sidebar-btn"><i class="fas fa-bars"></i></button>
        <div class="head-menu">
            <button id="sidebar-close-button"><i class="fas fa-arrow-left"></i></button>
            <ul class="header-links">
                <li>
                    <a href="">link</a>
                    <div class="submenu">
                        <ul>
                            <li><a href="#">Haircuts and Hairdressing</a></li>
                            <li><a href="#">Blow Dry</a></li>
                            <li><a href="#">Ladies' Hair Colouring & Highlights</a></li>
                            <li><a href="#">Ladies' Brazilian Blow Dry</a></li>
                            <li><a href="#">Balayage & Ombre</a></li>
                            <li><a href="#">Men's Haircut</a></li>
                            <li><a href="#">See all hair treatments</a></li>
                        </ul>
                        <a href="#" class="img-card">
                            <img src="https://cdn1.treatwell.net/images/view/v2.i1549312.w680.h340.x2EC4B123.jpg" alt="" />
                            <h3>Last minute? Cuts near you</h3>
                        </a>
                        <a href="#" class="img-card">
                            <img src="https://cdn1.treatwell.net/images/view/v2.i1549312.w680.h340.x2EC4B123.jpg" alt="" />
                            <h3>Hair treatments: the lowdown</h3>
                        </a>
                    </div>
                </li>
                <li>
                    <a href="">link</a>
                </li>
                <li>
                    <a href="">link</a>
                </li>
            </ul>
            <div class="auth">
                <button type="button" class="btn btn-dark btn-sm" data-backdrop="false" data-toggle="modal" data-target="#loginModal">Login</button>
                <button type="button" class="btn btn-dark btn-sm" data-backdrop="false" data-toggle="modal" data-target="#registerModal">Register</button>
            </div>

            <div class="auth-modal modal fade" id="loginModal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h5 class="modal-title">Login Account</h5>
                        </div>
                        <div class="modal-body">
                            <form action="">
                                <div class="form-group">
                                    <label for="email">Email:</label>
                                    <input type="email" class="form-control" id="email" placeholder="example@exmaple.com" />
                                </div>
                                <div class="form-group">
                                    <label for="password">Password:</label>
                                    <input type="password" class="form-control" id="password" placeholder="Enter Your Password" />
                                </div>
                                <button type="button" class="btn btn-dark btn-block">Login</button>
                            </form>
                            <div class="auth-links">
                                <a href="./forgot.html">Forgot Your Password?</a>
                                <a href="./register.html">Create New Account</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="auth-modal modal fade" id="registerModal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h5 class="modal-title">Login Account</h5>
                        </div>
                        <div class="modal-body">
                            <form action="">
                                <div class="form-group">
                                    <label for="firstName">First Name</label>
                                    <input type="text" class="form-control" id="firstName" placeholder="Enter Your First Name" />
                                </div>
                                <div class="form-group">
                                    <label for="last_name">Last Name</label>
                                    <input type="text" class="form-control" id="last_name" placeholder="Enter Your Last Name" />
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" id="email" placeholder="example@exmaple.com" />
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" id="password" placeholder="Enter Your Password" />
                                </div>
                                <div class="form-group">
                                    <label for="confirm_password">Confirm Password</label>
                                    <input type="password" class="form-control" id="confirm_password" placeholder="Repeat Your Password" />
                                </div>
                                <button type="button" class="btn btn-dark btn-block">Register</button>
                            </form>
                            <div class="auth-links">
                                <a href="./forgot.html">Forgot Your Password?</a>
                                <a href="./register.html">Create New Account</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="account d-none">
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQiowrV0bVwR6zOZJ7ZDjNS29YakE2QpD0L_dZTgeVh6j4b4N3u" alt="" />
                <p class="name">Hello, Ali</p>
                <i class="fa fa-angle-down"></i>
                <ul class="account-options">
                    <li><a href="#">Profile</a></li>
                    <li><a href="#">Logout</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="menu-container" >
        <ul class="menu">
            <li class="">
                <a class="" href="http://beautyday.ir/entity/list/by_category/1">
                    مراکز زیبایی
                </a>
            </li>
            <li class="">
                <a class="" href="http://beautyday.ir/entity/list/by_category/3">
                    مراکز اسپا
                </a>
            </li>
            <li class="">
                <a class="" href="http://beautyday.ir/core/page/show/%D8%A7%D8%B1%D8%AA%D8%A8%D8%A7%D8%B7-%D8%A8%D8%A7-%D9%85%D8%A7">
                    تماس با ما
                </a>
                <div class="submenu">
                    <ul>
                        <li class="">
                            <a class="" href="http://beautyday.ir/core/page/show/%D8%B1%D8%A7%D9%87%DA%A9%D8%A7%D8%B1%D9%87%D8%A7%DB%8C-%D8%B3%D8%A7%D8%B2%D9%85%D8%A7%D9%86%DB%8C-%D9%81%D8%B1%D8%A7%DB%8C%D8%A7%D8%AF">
                                راهکار سازمانی
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>
